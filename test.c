#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main() {
  int max = 100000;
  for (int i = 0; i < max; ++i) {
    int rv = fork();
    if (rv == -1) {
      printf("Fork failed at child number %d: %s", i, strerror(errno));
      return 1;
    } else if (rv == 0) {
      // In child. return and exit.
      return 0;
    } else {
      // In parent; keep going.
    }
  }
  printf("Finished spawning %d children", max);
  return 0;
}
